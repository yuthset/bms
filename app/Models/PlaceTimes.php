<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class PlaceTimes extends Model {
    public $table = 'tbl_place_times';

    public function times() {
        return $this->hasMany('App\Models\Time');
    }

    public function places() {
        return $this->hasMany('App\Models\Place');
    }
}
