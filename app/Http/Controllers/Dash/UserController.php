<?php

namespace App\Http\Controllers\Dash;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;

use yajra\Datatables\Datatables;

use App\Models\User;

class UserController extends Controller {

    public function index() {

        $data['users'] = User::latest()->get();
        $data['latest'] = User::latest()->first();
        return view('dash.user.index', $data);
    }

    public function create() {
        $data['types'] = User::lists('type', 'type');
        return view('dash.user.create', $data);
    }

    public function store(UserRequest $request) {
        $user = $request->all();
        $user['password'] = bcrypt($request->password);
        User::create($user);
        return redirect('dash/user');
    }

    public function show(){

        // return view('dash.user.edit', $users);
    }

    public function edit($id){
        $data['user'] = User::findOrFail($id);
        $data['types'] = User::lists('type', 'type');
        switch (User::where('id', $id)->pluck('type')) {
            case 'admin':
                $data['type'] = 'admin';
                break;
            case 'user':
                $data['type'] = 'user';
                break;
            default:
            case 'manager':
                $data['type'] = 'manager';
                break;
        }
        return view('dash.user.edit', $data);
    }

    public function update(UserRequest $request, $id){
        $user = User::findOrFail($id);
        $user->update($request->all());

        return redirect('dash/user');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
