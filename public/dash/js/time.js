$(function(){
	$(document).on('click', '.btnAddNewTime', function(){
		var url = '/dash/time/create';
		$(location).attr('href', url);	
	})

	$(document).on('click', '.btnUpdateTime', function(){
		var id = $(this).parents('tr').attr('data-id');
		var url = "/dash/time/"+ id +"/edit";
		$(location).attr("href", url);
	});

	$(document).on('click', '.btnDeleteTime', function(){
		var id = $(this).parents('tr').attr('data-id');
		var url = "/dash/time/" + id;
		if(confirm("Do you want to delete this time?") == true){
			$(location).attr("href", url);
			return true;
		} 
		else {
			return false;
		}
	});



	// $(".cbxAll").click(function(){
	// 		if($(".cbxAll").attr("checked", true)) {
	// 		$(".cbxTime").prop("checked", true);
	// 	} else {
	// 		alert("ss");
	// 		$('.cbxTime').removeAttr('checked');
	// 		$(".cbxTime").prop("checked", false);
	// 	}
	// });

	$(document).on('change', '.cbxAll', function(){
		if ($('.cbxAll').is(':checked')) {
			$('.cbxTime').attr('checked', true);
		} else  {
			$('.cbxTime').attr('checked', false);
		}
	});

	//add option to selTimeFrom
	

	// $(document).on('click', '#submitTime', function(){
	// 	alert($('#selTime').val());
	// });
});