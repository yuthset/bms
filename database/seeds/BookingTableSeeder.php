<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class BookingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=1;$i<=10;$i++) {
            DB::table('tbl_booking')->insert([
                'type_trip' => 1,
                /*'place_from'=> 1,
                'place_to' => $i+1,
                'time_stop' => $i,
                'time_start' => $i+1,*/
                //'time_id' => 1,
                'seats_amount' => 1,
                'branch_id' => 2,
//                'seats_price' => 15000,
                'user_id' => 0,
                'customer_id' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }
    }
}
