<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class DepartureTableSeeder extends Seeder {

    public function run() {
        for($i=2; $i<=10; $i++) {
        	DB::table('tbl_departures')->insert([
            'time_start' => 1,
            'time_stop' => $i,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        	]);
        }
    }
}
