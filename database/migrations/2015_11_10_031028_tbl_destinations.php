<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblDestinations extends Migration {
 
    public function up() {
        Schema::create('tbl_destinations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('from')->unsigned();
            $table->integer('to')->unsigned();
            $table->decimal('price',10,2)->unsigned();
            $table->timestamps();

            $table->foreign('from')->references('id')->on('tbl_places')->onDelete('cascade');
            $table->foreign('to')->references('id')->on('tbl_places')->onDelete('cascade');
        });
    }

    public function down() {
        Schema::drop('tbl_destinations');
    }
}
