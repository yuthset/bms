    <div class="form-group">
        <label for="name">Name: </label>
        {!! Form::text('name', null, ['class'=>'form-control']) !!}
    </div>

    <div class="form-group">
        <label for="address">Address: </label>
        {!! Form::textarea('address', null, ['class'=>'form-control']) !!}
    </div>

    <div class="form-group">
        <label for="control_by">Control By: </label>
        {!! Form::select('user_id', $users, null, ['class'=>'form-control'])!!}
    </div>

    <div class="form-group">
        <label for="phone">Phone: </label>
        {!! Form::text('phone', null, ['class'=>'form-control']) !!}
    </div>

    <button type="submit" class="btn btn-primary">{{ $submitText }}</button>