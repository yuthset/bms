@extends('dash')

@section('title', 'Modify Branch')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <h1 class="page-header">Modify Branch Information</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-md-8">
            {!! Form::model($branch, ['method' => 'PATCH', 'action' => ['Dash\BranchController@update', $branch->id], 'class'=>'form']) !!}
                @include('dash.branch.form', ['submitText' => 'Update Branch'])
            {!! Form::close() !!}
            <hr/><br/>
        </div>
    </div>

@stop