@extends('dash')

@section('title', 'Details Branch')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <h1>{!! $branch->name !!}</h1>

            <p class="lead">
                by <a href="#">{!! $branch->created_by !!}</a>
            </p>

            <hr>
            <p>
                <span class="glyphicon glyphicon-time"></span> Posted on {!! $branch->created_at !!}
            </p>
            <hr>

            <img class="img-responsive" src="http://placehold.it/900x300" alt="">
            <hr>

            <p class="lead">No information</p>
            <hr>

            <div class="well">
                <h4>Address: <p>{!! $branch->address !!}</p></h4>

            </div>
            <hr>
        </div>
    </div>

@stop