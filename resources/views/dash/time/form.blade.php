	@include('errors.validate')

    <div class="form-group">
    	{!! Form::label('time', 'Time') !!}
    	<div class="input-group clockpicker" data-placement="left" data-align="top" data-autoclose="true">

    	{!! Form::text('time',  null , ['class' => 'form-control']) !!}
		 	<span class="input-group-addon">
		  		<span class="glyphicon glyphicon-time"></span>
			</span>
		</div>
		
    </div>

    <div class="form-group">
    	<button type='submit' class='btn btn-primary' id="submitTime">{{ $submitText }}</button>	
    </div>

    @push('scripts')

	    {!! HTML::style('dash/css/bootstrap-clockpicker.min.css') !!}
	    {!! HTML::script('dash/js/bootstrap-clockpicker.min.js') !!}

	    <script type="text/javascript">
	        $('.clockpicker').clockpicker({
	        	placement: 'bottom',
	        	align: 'left'
	        });
	    </script>

	@endpush