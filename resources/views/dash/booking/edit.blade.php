@extends('dash')

@section('content')
    {!! Form::model(['url'=>'dashboard/booking']) !!}
        @include('dash.booking.form',['submitButtonText'=>'Update!','pageHeader'=>'Update Booking data!'])
    {!! Form::close() !!}
@stop
