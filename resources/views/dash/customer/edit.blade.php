@extends('dash')


@section('content')


    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Edit Users</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-8">
                    @include('errors.validate')
                    {!! Form::model($user, ['method' => 'PATCH', 'action' => ['Dash\UserController@update', $user->id]])!!}
                    {{-- {!! Form::model($user, ['route' => ['dash.user.update', $user->id]])!!} --}}
                        @include('dash.user.form', ['submittype' => 'Update'])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>

    </div>
    <hr/><br/>
@stop
