@extends('dash')


@section('content')
    <table class="table table-bordered" id="customerTable">
        <thead>
            <tr>
                <th>Id</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Username</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Created At</th>
                <th>Updated At</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Id</td>
                <td>First Name</td>
                <td>Last Name</td>
                <td>Username</td>
                <td>Email</td>
                <td>Phone</td>
                <td>Created At</td>
                <td>Updated At</td>
            </tr>
        </tbody>
    </table>

@stop

@push('scripts')
    {!! HTML::style('dash/css/datatables.css') !!}
    {!! HTML::script('dash/js/datatables.js') !!}

    <script>
    $(function() {
        $('#customerTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('dash.customer.json') !!}',
            columns: [
                { data: 'id', name: 'id'},
                { data: 'firstname', name: 'firstname' },
                { data: 'lastname', name: 'lastname' },
                { data: 'username', name: 'username' },
                { data: 'email', name: 'email' },
                { data: 'phone', name: 'phone' },
                { data: 'created_at', name: 'created_at' },
                { data: 'updated_at', name: 'updated_at' }
            ]
        });
    });
    </script>

@endpush

