@extends('home')

@section('content')
    <section>
        <div class="container">
            <div class="slider">
                <ul class="rslides" id="slider1">
                    <li><img src="images/banner/2.jpg" alt=""></li>
                    <li><img src="images/banner/1.jpg" alt=""></li>
                </ul>
            </div>

            <h2>Welcome to BMS</h2>

            <p>Content static here</p>

            <div class="row">
                <div class="col-md-8">
                    test
                </div>
                <div class="col-md-4">
                    test
                </div>
            </div>

            <div class="clearfix"></div>
        </div>
    </section>
@stop

@section('bottom_content')

@stop
